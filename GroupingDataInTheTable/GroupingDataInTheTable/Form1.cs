﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;


namespace GroupingDataInTheTable
{
    public partial class Form1 : Form
    {
        private BindingSource bindingSource1 = new BindingSource();

        public Form1()
        {
            InitializeComponent();
            loadDefaultData();
        }

        private void loadDefaultData()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionStringDB"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("SELECT * FROM ShipmentRegistration", connection))
                {
                    command.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(command))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            dataGridView2.DataSource = dt;
                        }
                    }
                }
            }
        }

        private void clickColumnDateRegistration()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionStringDB"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("Select DateRegistration, sum(Quantity) as Quantity, sum(Cost) as Cost from ShipmentRegistration group by DateRegistration; ", connection))
                {
                    command.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(command))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            dataGridView2.DataSource = dt;
                        }
                    }
                }
            }
        }

        private void clickColumnOrganization()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionStringDB"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("Select Organization, sum(Quantity) as Quantity, sum(Cost) as Cost from ShipmentRegistration group by Organization; ", connection))
                {
                    command.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(command))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            dataGridView2.DataSource = dt;
                        }
                    }
                }
            }
        }

        private void clickColumnCity()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionStringDB"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("Select City, sum(Quantity) as Quantity, sum(Cost) as Cost from ShipmentRegistration group by City; ", connection))
                {
                    command.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(command))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            dataGridView2.DataSource = dt;
                        }
                    }
                }
            }
        }

        private void clickColumnCountry()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionStringDB"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("Select Country, sum(Quantity) as Quantity, sum(Cost) as Cost from ShipmentRegistration group by Country; ", connection))
                {
                    command.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(command))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            dataGridView2.DataSource = dt;
                        }
                    }
                }
            }
        }

        private void clickColumnManager()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionStringDB"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("Select Manager, sum(Quantity) as Quantity, sum(Cost) as Cost from ShipmentRegistration group by Manager; ", connection))
                {
                    command.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(command))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            dataGridView2.DataSource = dt;
                        }
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loadDefaultData();
        }

        private void dataGridView2_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Console.WriteLine("clicked");

            DataGridViewColumn newColumn = dataGridView2.Columns[e.ColumnIndex];
            DataGridViewColumn oldColumn = dataGridView2.SortedColumn;

            if (oldColumn == newColumn &&
                dataGridView2.Columns[0].Name == "DateRegistration")
            {
                clickColumnDateRegistration();
            }
            else { }

            if (oldColumn == newColumn &&
                dataGridView2.Columns[1].Name == "Organization")
            {
                clickColumnOrganization();
            }
            else { }
        }


        // select * from TABLE GROUPBY dataGridView2.Columns[0].Name

        // GROUPBY %clicked_header%
    }
}
